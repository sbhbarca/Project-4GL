package tn.esprit.traducteur;

import javax.ejb.Remote;

@Remote
public interface TraducteurRemote {

	public String traduire(String mot);
}
