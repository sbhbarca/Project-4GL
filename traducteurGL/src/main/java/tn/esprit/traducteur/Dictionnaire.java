package tn.esprit.traducteur;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

@Singleton
@LocalBean
public class Dictionnaire {

	private Map<String, String> values;


	@PostConstruct
	public void init(){
		values = new HashMap<>();
		values.put("Bonjour", "Good Morning");
		values.put("Salut", "Hello");
		values.put("Bonsoir", "Good Evening");
	}
	
	public boolean isFrench(String mot){
		return values.containsKey(mot);
	}
	
	public boolean isEnglish(String mot){
		return values.containsValue(mot);
	}
	
	public String traduireFrEn(String mot){
		return values.get(mot);
	}
	
	public String traduireEnFr(String mot){
		Set<String> keys = values.keySet();
		for (String key : keys) {
			if (values.get(key).equals(mot)) {
				return key;
			}
		}
		return null;
	}
	
	
	
}
