package tn.esprit.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.entities.User;

/**
 * Session Bean implementation class ServiceUser
 */
@Stateless

public class ServiceUser implements ServiceUserRemote, ServiceUserLocal {

	@PersistenceContext
	EntityManager em;

	/**
	 * Default constructor.
	 */
	public ServiceUser() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addUser(User u) {
		// TODO Auto-generated method stub
		em.persist(u);
	}

	@Override
	public User authentification(String login, String pwd) {
		// TODO Auto-generated method stub
		User u = null;
		TypedQuery<User> query = em
				.createQuery("select u from User u where u.login = :l and u.password like :p", User.class)
				.setParameter("l", login).setParameter("p", pwd);
		try {
			u = query.getSingleResult();
			return u;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("user null");
		}
		return null;
	}

}
