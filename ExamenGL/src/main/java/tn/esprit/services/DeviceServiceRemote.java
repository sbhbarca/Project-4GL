package tn.esprit.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.entities.Device;

@Remote
public interface DeviceServiceRemote {

	public void addDevice(Device e);
	public void updateDevice(Device e);
	public void deleteDevice(String id);
	public Device getDevicebyUid(String id);
	public List<Device> getAllDevice();
	public void affecterDeviceEmployee(String deviceUniqueid,long matricule);
	public List<Device> getDevicesByEmployee(long matricule);
}
