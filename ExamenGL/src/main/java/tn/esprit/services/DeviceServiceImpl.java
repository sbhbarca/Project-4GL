package tn.esprit.services;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.entities.Device;
import tn.esprit.entities.Employee;

/**
 * Session Bean implementation class DeviceServiceImpl
 */
@Stateless

public class DeviceServiceImpl implements DeviceServiceRemote,DeviceServiceLocal {

	@PersistenceContext
	EntityManager em;
	@EJB
	EmployeeServiceLocal se;
    /**
     * Default constructor. 
     */
    public DeviceServiceImpl() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addDevice(Device e) {
		// TODO Auto-generated method stub
		em.persist(e);
		
	}

	@Override
	public void updateDevice(Device e) {
		// TODO Auto-generated method stub
		em.merge(e);
	}

	@Override
	public void deleteDevice(String id) {
		// TODO Auto-generated method stub
		em.remove(getDevicebyUid(id));
	}

	@Override
	public Device getDevicebyUid(String id) {
		// TODO Auto-generated method stub
		return em.find(Device.class, id);
	}

	@Override
	public List<Device> getAllDevice() {
		// TODO Auto-generated method stub
		Query q = em.createQuery("select d from Device d",Device.class);
		return q.getResultList();
	}

	
	@Override
	public List<Device> getDevicesByEmployee(long matricule) {
		// TODO Auto-generated method stub
		//return em.fin, arg1)
		TypedQuery<Device> query = em.createQuery("select d from Device d where d.employee.matricule =:a",Device.class);
		query.setParameter("a", matricule);
		return query.getResultList();
	}

	@Override
	public void affecterDeviceEmployee(String deviceUniqueid, long matricule) {
		// TODO Auto-generated method stub
		Employee e = se.getEmpByMatricule(matricule);
		Device d = getDevicebyUid(deviceUniqueid);
		d.setEmployee(e);
		updateDevice(d);
		
		
	}

}
