import java.net.NetworkInterface;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entities.Employee;
import tn.esprit.entities.EmployeeType;
import tn.esprit.services.DeviceServiceRemote;
import tn.esprit.services.EmployeeServiceRemote;

public class ExamenGlTestAffectation {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub
		InitialContext context = new InitialContext();
		DeviceServiceRemote ds = (DeviceServiceRemote) context.lookup("ExamenGL/DeviceServiceImpl!tn.esprit.services.DeviceServiceRemote");
		ds.affecterDeviceEmployee("000a", 3);
		
	}

}
