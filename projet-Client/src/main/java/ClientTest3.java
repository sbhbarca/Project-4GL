import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entities.Ecole;
import tn.esprit.entities.Salle;
import tn.esprit.services.EcoleServiceRemote;
import tn.esprit.services.SalleServiceRemote;

public class ClientTest3 {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub
		InitialContext context = new InitialContext();
		SalleServiceRemote salleservice = (SalleServiceRemote) context.lookup
				("TP/ServiceSalleImpl!tn.esprit.services.SalleServiceRemote");

		EcoleServiceRemote es = (EcoleServiceRemote) context.lookup
				("TP/EcoleServiceImpl!tn.esprit.services.EcoleServiceRemote");
		
		
		Salle sale = salleservice.findSalleById(12);
		
		List<Salle> lsal = new ArrayList<>();
		lsal.add(sale);

		Ecole ec = es.findEcoleById(3);
		
		ec.setListSalle(lsal);
		es.updateEcole(ec);

	}

}
