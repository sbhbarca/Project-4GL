import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entities.Ecole;
import tn.esprit.services.EcoleServiceRemote;

public class ClientTest {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub
		InitialContext context = new InitialContext();
		EcoleServiceRemote es = (EcoleServiceRemote) context.lookup
				("TP/EcoleServiceImpl!tn.esprit.services.EcoleServiceRemote");
		Ecole e = new Ecole();
		e.setName("Esprit-Prepa");
		es.addEcole(e);
		
		e = new Ecole();
		e.setName("ESB");
		es.addEcole(e);
		
		Ecole ecole = es.findEcoleById(2);
		
		System.out.println("Affichage dont id = 2 "+ecole.getName());
		
		es.deleteEcole(2);
		
		List<Ecole> lecoles = es.getAllEcole();
		
		for (Ecole ecole2 : lecoles) {
			System.out.println(ecole2.getName());
		}
		
		
		
		

	}

}
