package tn.esprit.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.entities.Enseignant;
import tn.esprit.entities.TypeEns;
import tn.esprit.services.EnsServiceLocal;

@ManagedBean
@ViewScoped
public class EnseignantBean {
	private Enseignant ens;
	@EJB
	EnsServiceLocal enService;
	
	private TypeEns[] types;
	
	private boolean etat;
	List<Enseignant> lens = new ArrayList<>();

	
	
	public List<Enseignant> getLens() {
		return lens;
	}

	public void setLens(List<Enseignant> lens) {
		this.lens = lens;
	}

	@PostConstruct
	public void init() {
		ens = new Enseignant();
		types = TypeEns.values();
		lens = enService.getAllEns();
		etat =false;
	}

	public void addEns() {
		enService.addEns(ens);
		ens = new Enseignant();
	}

	public Enseignant getEns() {
		return ens;
	}

	public void setEns(Enseignant ens) {
		this.ens = ens;
	}
	
	public String preUpdate(Enseignant e){
		etat = true;
		ens = enService.findEnsById(e.getIdEns());
		ens.setName(e.getName());
		ens.setMail(e.getMail());
		ens.setDateNaissance(e.getDateNaissance());
		return null;
		
	}
	
	public void update(){
		enService.updateEns(ens);
		lens = enService.getAllEns();
		etat = false;
	}
	
	
	
	public String remove(long id){
		enService.deleteEns(id);
		lens = enService.getAllEns();
//		for (Enseignant enseignant : lens) {
//			if (enseignant.getIdEns() == id) {
//				lens.remove(enseignant);
//				break;
//			}
//		}
		return null;
		
	}

	public boolean isEtat() {
		return etat;
	}

	public void setEtat(boolean etat) {
		this.etat = etat;
	}

	public TypeEns[] getTypes() {
		return types;
	}

	public void setTypes(TypeEns[] types) {
		this.types = types;
	}



}
