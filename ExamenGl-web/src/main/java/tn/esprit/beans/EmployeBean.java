package tn.esprit.beans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import tn.esprit.entities.Device;
import tn.esprit.entities.Employee;
import tn.esprit.entities.EmployeeType;
import tn.esprit.services.DeviceServiceLocal;
import tn.esprit.services.EmployeeServiceLocal;

@ManagedBean
@RequestScoped
public class EmployeBean {

	@EJB
	EmployeeServiceLocal empservice;
	
	@EJB
	DeviceServiceLocal deviceservice;
	
	
	private long matriculeEmp;
	private String idDevice;
	
	private List<Device> lDeviceByEmp;
	
	private List<Employee> lemp;
	
	private List<Device> ldevices;
	
	@ManagedProperty("#{loginBean.employe}")
	private Employee emp;
	
	public Employee getEmp() {
		return emp;
	}

	public void setEmp(Employee emp) {
		this.emp = emp;
	}

	@PostConstruct
	public void init(){
		lemp = empservice.getAllEmp();
		ldevices = deviceservice.getAllDevice();
		if (emp.getEmployeetype().equals(EmployeeType.EMP)) {
			lDeviceByEmp = deviceservice.getDevicesByEmployee(emp.getMatricule());
		}
	}

	public List<Employee> getLemp() {
		return lemp;
	}

	public void setLemp(List<Employee> lemp) {
		this.lemp = lemp;
	}

	public List<Device> getLdevices() {
		return ldevices;
	}

	public void setLdevices(List<Device> ldevices) {
		this.ldevices = ldevices;
	}

	public List<Device> getlDeviceByEmp() {
		return lDeviceByEmp;
	}

	public void setlDeviceByEmp(List<Device> lDeviceByEmp) {
		this.lDeviceByEmp = lDeviceByEmp;
	}

	public long getMatriculeEmp() {
		return matriculeEmp;
	}

	public void setMatriculeEmp(long matriculeEmp) {
		this.matriculeEmp = matriculeEmp;
	}

	public String getIdDevice() {
		return idDevice;
	}

	public void setIdDevice(String idDevice) {
		this.idDevice = idDevice;
	}
	
	public void affecter(){
		deviceservice.affecterDeviceEmployee(idDevice, matriculeEmp);
		
	}
	
}
