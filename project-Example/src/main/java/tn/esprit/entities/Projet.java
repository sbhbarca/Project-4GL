package tn.esprit.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Projet
 *
 */
@Entity
public class Projet implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idProjet;
	private String nomProjet;
	@ManyToOne
	private Employer employee;
	@OneToMany(mappedBy="projet")
	private List<Tache> lsttaches;

	public List<Tache> getLsttaches() {
		return lsttaches;
	}

	public void setLsttaches(List<Tache> lsttaches) {
		this.lsttaches = lsttaches;
	}

	public Employer getEmployee() {
		return employee;
	}

	public void setEmployee(Employer employee) {
		this.employee = employee;
	}

	public int getIdProjet() {
		return idProjet;
	}

	public void setIdProjet(int idProjet) {
		this.idProjet = idProjet;
	}

	public String getNomProjet() {
		return nomProjet;
	}

	public void setNomProjet(String nomProjet) {
		this.nomProjet = nomProjet;
	}

	private static final long serialVersionUID = 1L;

	public Projet() {
		super();
	}

}
