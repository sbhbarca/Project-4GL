package tn.esprit.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Tache
 *
 */
@Entity
@IdClass(Tachepk.class)
public class Tache implements Serializable {

	@ManyToOne
	@JoinColumn(name = "idEmployee", referencedColumnName = "id", insertable = false, updatable = false)
	private Employer employee;
	@ManyToOne
	@JoinColumn(name = "idProjet", referencedColumnName = "idProjet", insertable = false, updatable = false)
	private Projet projet;
	private int duree;
 
	@Id
	private Integer idEmployee;
	@Id
	private int idProjet;
	
	public Employer getEmployee() {
		return employee;
	}

	public void setEmployee(Employer employee) {
		this.employee = employee;
	}

	public Projet getProjet() {
		return projet;
	}

	public void setProjet(Projet projet) {
		this.projet = projet;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

//	public Tachepk getTachepk() {
//		return tachepk;
//	}
//
//	public void setTachepk(Tachepk tachepk) {
//		this.tachepk = tachepk;
//	}

//	@EmbeddedId
//	private Tachepk tachepk;
	private static final long serialVersionUID = 1L;

	public Tache() {
		super();
	}

}
