package tn.esprit.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Adresse
 *
 */

@Embeddable
public class Adresse implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Adresse() {
		super();
	}
   
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	private String ville;
	private String rue;
}
