package tn.esprit.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Cours
 *
 */
@Entity

public class Cours implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long idCours;
	private String nom;
	@ManyToOne 
	private Salle salle ;
	@ManyToOne 
	private Enseignant enseignant ;
	public Salle getSalle() {
		return salle;
	}
	public void setSalle(Salle salle) {
		this.salle = salle;
	}
	public Enseignant getEnseignant() {
		return enseignant;
	}
	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	private static final long serialVersionUID = 1L;

	public Cours() {
		super();
	}   
	public long getIdCours() {
		return this.idCours;
	}

	public void setIdCours(long idCours) {
		this.idCours = idCours;
	}
   
}
