package tn.esprit.entities;

import java.io.Serializable;
import java.lang.Long;

/**
 * ID class for entity: DateEnseignant
 *
 */ 
public class DateEnseignantPK  implements Serializable {   
   
	         
	private long idEnseignant;         
	private long idEco;
	private static final long serialVersionUID = 1L;

	public DateEnseignantPK() {}

	

	public long getIdEnseignant() {
		return this.idEnseignant;
	}

	public void setIdEnseignant(long idEnseignant) {
		this.idEnseignant = idEnseignant;
	}
	

   
	public long getIdEco() {
		return idEco;
	}



	public void setIdEco(long idEco) {
		this.idEco = idEco;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idEco ^ (idEco >>> 32));
		result = prime * result + (int) (idEnseignant ^ (idEnseignant >>> 32));
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DateEnseignantPK other = (DateEnseignantPK) obj;
		if (idEco != other.idEco)
			return false;
		if (idEnseignant != other.idEnseignant)
			return false;
		return true;
	}




   
   
}
