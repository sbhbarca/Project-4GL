package tn.esprit.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Salle
 *
 */
@Entity

public class Salle implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long idSalle;
	private String nom;
	@OneToOne
	private Enseignant enseignant ;
	public Enseignant getEnseignant() {
		return enseignant;
	}
	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	private static final long serialVersionUID = 1L;

	public Salle() {
		super();
	}   
	public long getIdSalle() {
		return this.idSalle;
	}

	public void setIdSalle(long idSalle) {
		this.idSalle = idSalle;
	}
   
}
