package tn.esprit.services;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;

import tn.esprit.entities.Enseignant;

@Remote
public interface EnsServiceRemote {

	public void addEns(Enseignant c);
	public Enseignant updateEns(Enseignant c);
	public Enseignant findEnsById(long id);
	public Enseignant findEnsByName(String name);
	public void deleteEns(long id);
	public List<Enseignant> getAllEns();
}
