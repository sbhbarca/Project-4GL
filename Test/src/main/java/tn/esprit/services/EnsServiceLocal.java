package tn.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.entities.Enseignant;

@Local
public interface EnsServiceLocal {

	public void addEns(Enseignant c);
	public Enseignant updateEns(Enseignant c);
	public Enseignant findEnsById(long id);
	public Enseignant findEnsByName(String name);
	public void deleteEns(long id);
	public List<Enseignant> getAllEns();
	
	public Enseignant authentifier(long login, String name);
}
