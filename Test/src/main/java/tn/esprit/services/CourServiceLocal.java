package tn.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.entities.Cours;

@Local
public interface CourServiceLocal {

	public void addCour(Cours c);
	public Cours updateCour(Cours c);
	public Cours findCourById(long id);
	public Cours findCourByName(String name);
	public void deleteCour(long id);
	public List<Cours> getAllCour();
}
