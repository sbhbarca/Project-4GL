package tn.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.entities.Ecole;

@Local
public interface EcoleServiceLocal {
	public void addEcole(Ecole e);

	public Ecole updateEcole(Ecole e);

	public Ecole findEcoleById(long id);

	public Ecole findEcoleByName(String name);

	public void deleteEcole(long id);

	public List<Ecole> getAllEcole();
}
