package tn.esprit.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.entities.Salle;
@Stateless
public class ServiceSalleImpl implements SalleServiceLocal, SalleServiceRemote {

	@PersistenceContext
	EntityManager em;

	@Override
	public void addSalle(Salle s) {
		// TODO Auto-generated method stub
		em.persist(s);

	}

	@Override
	public Salle updateSalle(Salle s) {
		// TODO Auto-generated method stub
		return em.merge(s);
	}

	@Override
	public Salle findSalleById(long id) {
		// TODO Auto-generated method stub
		return em.find(Salle.class, id);
	}

	@Override
	public Salle findSalleByName(String name) {
		// TODO Auto-generated method stub
		TypedQuery<Salle> query = em.createQuery("select s from Salle s where s.nom like :m", Salle.class);
		query.setParameter("m", name);
		return query.getSingleResult();
	}

	@Override
	public void deleteSalle(long id) {
		// TODO Auto-generated method stub
		em.remove(findSalleById(id));

	}

	@Override
	public List<Salle> getAllSalle() {
		// TODO Auto-generated method stub
		Query query = em.createQuery("select s from Salle");
		return query.getResultList();
	}

}
